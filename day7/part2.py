import numpy as np
fuel = lambda x: x*(x+1) // 2
diff = lambda x,y: np.rint(np.abs(np.subtract(x,y)))
def process_data(crabarray: np.ndarray):
    mean = np.mean(crabarray)
    ceilingMean = np.ceil(mean)
    floorMean = np.floor(mean)
    diffCeiling = diff(crabarray,ceilingMean)
    diffFloor = diff(crabarray,floorMean)
    fuelCeiling = np.sum(fuel(diffCeiling))
    fuelFloor = np.sum(fuel(diffFloor))
    return np.amin([fuelCeiling,fuelFloor])

if __name__ == "__main__":
    import sys
    fileName = sys.argv[1]
    input = np.genfromtxt(fileName,dtype=int,delimiter=",")
    print(process_data(input))


