import numpy as np
import part2
def test():
    input = np.array([16,1,2,0,4,2,7,1,2,14])
    assert part2.process_data(input) == 168.0
