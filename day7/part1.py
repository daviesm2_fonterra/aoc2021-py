import numpy as np
def process_data(crabarray: np.ndarray):
    median = np.median(crabarray)
    diff = np.rint(np.abs(np.subtract(crabarray,median)))
    return np.sum(diff)
if __name__ == "__main__":
    import sys
    fileName = sys.argv[1]
    input = np.genfromtxt(fileName,dtype=int,delimiter=",")
    sum = process_data(input)
    print(sum)


